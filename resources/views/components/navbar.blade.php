<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bgNavCustom sticky-top" >
    <div class="container-fluid">
        <btn class="btn btnCustom" href="#"><i class="bi bi-camera biSize"></i></btn>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
            <ul class="navbar-nav position-custom">
                <li class="nav-item">
                <a class="customLink underlineHover" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                <a class="customLink underlineHover" href="#">Features</a>
                </li>
                <li class="nav-item">
                <a class="customLink underlineHover" href="#">Pricing</a>
                </li>
                <li class="rightPos">
                    <button type="button" class="btn btnCustomLog" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <i class="bi bi-door-open biSize"></i>
                    </button>
                </li>
                <li class="btnBar rightPosMenu">
                    <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#modalMenu">
                    <div class="btnBar1"></div>
                    <div class="btnBar2"></div>
                    <div class="btnBar3"></div>
                    </button>
                </li>
            </ul>
        </div>
    </div>
</nav>

