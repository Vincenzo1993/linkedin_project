const { forEach } = require("lodash");

let navbar = document.getElementById('navbar');

document.addEventListener('scroll', ()=>{
    // console.log(window.scrollY);
    let scrolled = window.scrollY;
    if(scrolled >=2){
        if(!navbar.classList.contains('navAnimation')){
            setTimeout(()=>{
                navbar.classList.remove('navAnimationBack')
            }, 1)
            navbar.classList.add('navAnimation')
        }
    }else{
        setTimeout(()=>{
            navbar.classList.remove('navAnimation')

        }, 1)
        navbar.classList.add('navAnimationBack')
    }
})

let btnBar = document.querySelector('.btnBar')
let btnBarDiv = document.querySelectorAll('.btnBar div')
let modalMenu = document.querySelector("#modalMenu")
let body = document.querySelector('body')


btnBar.addEventListener('click', ()=>{
    btnBarDiv.forEach((el)=>{
        el.classList.add('change')
    })
})

window.addEventListener("keydown", event => {
    if (event.keyCode === 27) {
        btnBarDiv.forEach((el)=>{
            el.classList.remove('change')
        })
    }
})

body.addEventListener('click', ()=>{
    if(!modalMenu.classList.contains('show')){
        btnBarDiv.forEach((el)=>{
            el.classList.remove('change')
        })
    }
})